#!/usr/bin/env python

from __future__ import print_function

import collections
import fileinput
import sys

plop = collections.defaultdict(list)

try:
  import fontTools.agl

  def map_character(c):
    try:
      return fontTools.agl.UV2AGL.get(ord(c), c)
    except:
      return c
except ImportError:
  print('WARNING: Package fonttools not found.', file=sys.stderr)
  # Don't have fonttools package.
  # TODO: 5 ---> five etc.
  character_mappings = {
    ' ': 'space',
  }

  def map_character(c):
    return character_mappings.get(c, c)

for word in fileinput.input():
  word = word.strip()
  length = len(word)
  string = ''
  glue = ''
  firstletter = ''

  if length < 6:
    yooniecode = 'uniE602'
  elif length > 11:
    yooniecode = 'uniE600'
  else:
    yooniecode = 'uniE601'
  firstletter = word[0]
  string = ','.join(map(map_character, word[1:]))

  line = '  <Ligature components="' + string + '" glyph="' + yooniecode + '"/>'

  plop[firstletter].append(line)

for ltr in plop:
  print('<LigatureSet glyph="' + ltr + '">')
  print("\n".join(plop[ltr]))
  print('</LigatureSet>' + "\n")
